const { user, customer, userPartner, cart } = require('../models')
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');
const secret = process.env.SECRET;
const uuidv4 = require("uuid");
const { Op } = require("sequelize");
class UserController {
    static async register(req, res, next) {
        //common user

        try {
            const { password, email, first_name, last_name, is_user_partner, address, phone_number } = req.body;
            const { business_partner_id } = req.params;

            const userFound = await user.findOne({ where: { email } });

            //create user_id and customer_id
            const user_id = uuidv4.v4();
            const customer_id = uuidv4.v4();

            //insert user to table users
            const val = { user_id, password, email, first_name, last_name }
            var createdUser = await user.create(val);
            delete createdUser.dataValues.password;

            if (!is_user_partner) {
                try {
                    const customerCreated = await customer.create({ user_id, customer_id });
                    await user.update({ customer_id }, { where: { user_id } });
                    cart.create({ customer_id })
                        .then((result) => {
                            console.log(result)
                        }).catch((err) => {
                            console.log(err)
                        });

                    const tokenData = { user_id, customer_id, email };
                    const token = jwt.sign(tokenData, secret);
                    if (business_partner_id) {
                        req.body.customer_id = customer_id;
                        next()
                    } else {
                        return res.send({ code: 200, msg: "success", access_token: token, data: createdUser });
                    }

                } catch (err) {
                    await user.destroy({ where: { user_id } })
                    res.send({ code: 500, msg: 'failed to create customer', error: err });
                }

            } else {
                const { role_id, email } = req.body;
                const user_partner_id = uuidv4.v4();

                try {
                    const createdUserPartner = await userPartner.create({ user_id, user_partner_id, role_id, email });
                    await user.update({ user_partner_id }, { where: { user_id } });
                    const tokenData = { user_id, user_partner_id, role_id: role_id };
                    const token = jwt.sign(tokenData, secret);

                    return res.send({ code: 200, msg: "success", access_token: token, data: { createdUser, createdUserPartner } });

                } catch (err) {
                    await user.destroy({ where: { user_id } })
                    return res.send({ code: 500, msg: "failed to create user partner", error: err })
                }
            }
        } catch (err) {
            next(err)
        }
    }

    static async deleteUserById(req, res) {
        const { deleteUserPartnerOnly, deleteCustomerOnly } = req.body;
        const { user_id } = req.params;

        try {
            const foundUser = await user.findOne({ where: { user_id } });
            if (foundUser) {
                if (!deleteUserPartnerOnly && !deleteCustomerOnly) {
                    try {
                        await user.destroy({ where: { user_id } });
                        return res.send({ code: 200, msg: "success" });
                    } catch (err) {
                        return res.send({ code: 500, msg: err })
                    }
                } else {
                    if (deleteUserPartnerOnly) {
                        try {
                            const foundUserPartner = await userPartner.findOne({ where: { user_id } });
                            if (foundUserPartner) {
                                await userPartner.destroy({ where: { user_id } });
                            } else {
                                return res.send({ code: 404, msg: "User Partner Not Found" })
                            }
                        } catch (err) {
                            return res.send({ code: 500, msg: err });
                        }
                    }
                    if (deleteCustomerOnly) {
                        try {
                            const foundCustomer = await customer.findOne({ where: { user_id } });
                            if (foundCustomer) {
                                await customer.destroy({ where: { user_id } });
                            } else {
                                return res.send({ code: 404, msg: "Customer Not Found" })
                            }
                            await customer.destroy({ where: { user_id } });
                        } catch (err) {
                            return res.send({ code: 500, msg: err });
                        }
                    }
                    res.send({ code: 200, msg: "success" })
                }
            } else {
                res.send({ code: 404, msg: "not found" });
            }
            return res.send({ code })
        } catch (Err) {

        }
    }

    static async updateUser(req, res) {

        try {
            const obj = req.body;
            const { user_id } = req.params;
            const { user_partner_id, update_customer, update_userPartner } = req.body;
            const foundUser = await user.findOne({ where: { user_id } });

            if (!foundUser) {
                return res.send({ code: 404, msg: "user not found", foundUser })
            } else {
                try {
                    await user.update(obj, { where: { user_id } });
                    if (!update_customer && !update_userPartner) {
                        const updatedUser = await user.findOne({ where: { user_id } })
                        delete updatedUser.dataValues.password
                        return res.send({ code: 200, msg: "success", data: updatedUser })
                    }

                    if (!!update_userPartner) {
                        try {
                            await userPartner.update(obj, { where: { user_id } });
                            const updatedUserPartner = await user.findOne({ where: { user_id } }, {
                                include: [{
                                    model: userPartner
                                }]
                            });
                            delete updatedUserPartner.dataValues.password
                            return res.send({ code: 200, msg: "success", data: updatedUserPartner })
                        } catch (err) {
                            res.send({ code: 500, msg: "Internal server error" + err })
                        }
                    } else if (!!update_customer) {
                        try {
                            const customer = await customer.update(obj, { where: { user_id } });
                            const updatedCustomer = await user.findOne({ where: { user_id } }, {
                                include: [{
                                    model: customer
                                }]
                            });
                            delete updatedCustomer.dataValues.password
                            return res.send({ code: 200, msg: "success", data: updatedCustomer })
                        } catch (err) {
                            res.send({ code: 500, msg: "Internal server error" + err })
                        }
                    }
                } catch (err) {
                    return res.send({ code: 500, msg: "Internal Error" })
                }

            }
        } catch (err) {
            return res.send({ code: 500, msg: err });
        }
    }

    static async login(req, res) {
        //varabel login_as customer or user_partner?
        const { email, password, is_user_partner } = req.body;
        try {
            const foundUser = await user.findOne({
                where: {
                    email
                },
                include: [{
                    model: is_user_partner ? userPartner : customer,
                    required: false
                }],
            })
            if (foundUser) {
                const isSame = await bcrypt.compare(password, foundUser.password);
                if (isSame) {
                    const { user_id, user_partner_id, customer_id } = foundUser.dataValues;
                    if (!Boolean(is_user_partner)) {
                        const tokenData = { user_id, customer_id };
                        const token = jwt.sign(tokenData, secret);
                        delete foundUser.dataValues["password"];
                        return res.send({ code: 200, token, data: foundUser, })
                    } else {
                        // console.log(isSame)
                        delete foundUser.dataValues["password"];
                        const { user_id, user_partner_id, userPartner } = foundUser.dataValues
                        const tokenData = { user_id, user_partner_id, role_id: userPartner.role_id, business_partner_id: userPartner.business_partner_id }
                        const token = jwt.sign(tokenData, secret);
                        return res.send({ code: 200, token, data: foundUser });
                    }
                }
                else {
                    return res.send({ code: 401, msg: "incorrect email/password" })
                }
            } else {
                return res.send({ code: 404, msg: "user not found" });
            }
        } catch (err) {
            next(err);
        }
    }
    static async getUserByEmailOrUserId(req, res) {
        const { email, user_id } = req.params;

        try {
            const foundUser = await user.findOne({
                where: {
                    [Op.or]: [
                        {
                            email
                        },
                        {
                            user_id
                        }
                    ]
                }
            });

            if (foundUser) {
                return res.send({ code: 200, msg: "Success", data: foundUser })
            } else {
                return res.send({ code: 404, msg: "Not Found" })
            }
        } catch (err) {
            return res.send({ code: 500, msg: err })
        }
    }
}
module.exports = UserController;