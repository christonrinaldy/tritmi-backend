const { order_detail, cart, service_detail } = require("../models")

class CartController {
    static async emptyCart(req, res, next) {
        try {
            const {cart_id} = req.body
            const {customer_id} = req.params
            await cart.update({business_partner_id: null}, {where: {customer_id}})
            await order_detail.update({cart_id: null}, {where: {cart_id}})

            res.send({code: 200, msg: "success"})
        } catch(err) {
            next(err)
        }
        
    } 
    static async getCartDetails(req,res,next) {
        try {
            const {customer_id} = req.params

            const cartDetails = await cart.findOne({
                where: {customer_id},
                include: [
                    {
                        model: order_detail
                    }
                ]
            })
            res.send({code: 200, msg: "success", data: cartDetails})
        } catch (error) {
            next(error)
        }
    }
}
module.exports = CartController