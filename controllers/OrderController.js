const { service_detail, order_detail, service, order, customer, user } = require("../models")
const uuidv4 = require("uuid");
const { Op } = require("sequelize/dist");

class OrderController {
    static async createOrder(req, res, next) {
        try {
            const { order_detail_ids } = req.body
            //cari service_id di service_details milik business_partner_id 
            var base_price = 0;
            const order_id = uuidv4.v4();

            if (true) {
                console.log("create Order", req.body)

                const foundedOrderDetails = await order_detail.findAll({
                    where: { order_detail_id: order_detail_ids },
                    include: [{
                        model: service_detail
                    }]
                })

                console.log("foundedOrderDetails: ", foundedOrderDetails)
                if (foundedOrderDetails.length !== order_detail_ids.length) {
                    return res.send({ code: 404, msg: "not found" })
                }


                foundedOrderDetails.map((val) => {
                    base_price += val.service_detail.offer_price;
                    if (val.order_id != null) {
                        return res.send({ code: 409, msg: "order detail is already used, order_id is not null", data: val })
                    }
                })


                req.body["base_price"] = Number(base_price);
                req.body["order_id"] = order_id;

                const createdOrder = await order.create(req.body);
                const updatedOrderDetails = await order_detail.update({ order_id, cart_id: null }, { where: { order_detail_id: order_detail_ids } });

                res.send({ code: 200, msg: "success", data: { createdOrder, updatedOrderDetails } });
            } else {
                try {
                    const createdOrder = await order.create({ order_id, ...req.body });
                    res.send({ code: 200, msg: "success", data: createdOrder });

                } catch (err) {
                    res.send({ code: 500, msg: "Internal Server Error", error: err, })
                }
            }


        } catch (err) {
            res.send({ code: 500, msg: "Internal Server Error", error: err })
        }
    }
    static async deleteOrderByOrderId(req, res) {
        const { order_id } = req.params
        try {
            const foundedOrder = await order.findOne({ where: { order_id } });

            if (!!foundedOrder) {
                const destroyedOrder = await order.destroy({ where: { order_id } });

                res.send({ code: 200, msg: "success", data: foundedOrder })
            } else {
                res.send({ code: 404, msg: "not found" })
            }

        } catch (err) {
            res.send({ code: 500, msg: "Internal Server Error", error: err })
        }
    }
    static async updateOrderById(req, res) {
        const { order_id } = req.params
        try {
            const foundedOrder = order.findByPk(order_id);

            if (foundedOrder) {
                await order.update(req.body, { where: { order_id } });
                const updatedOrder = await order.findOne({
                    where: {
                        order_id
                    }, include: [{
                        model: order_detail
                    }]
                })

                res.send({ code: 200, msg: "success", data: updatedOrder })
            } else {
                res.send({ code: 404, msg: "not found" })
            }

        } catch (err) {
            res.send({ code: 500, msg: "Internal Server Error", error: err })
        }
    }
    static async getOrderDetailByOrderId(req, res) {
        const { order_id } = req.params;
        try {
            const foundedOrder = await order.findOne({
                where: { order_id },
                include: [{
                    model: order_detail
                }]
            })
            return res.send({ code: 200, msg: "success", data: foundedOrder })
        } catch (err) {
            res.send({ code: 500, msg: "Internal Server Error", error: err })
        }
    }
    static async readOrderByBusinessPartnerIdAndTimeFilter(req, res) {
        const { business_partner_id, start_time, end_time } = req.params;
        try {
            const foundedOrder = await order.findAll({
                where: {
                    business_partner_id,
                    order_time: {
                        [Op.lt]: end_time,
                        [Op.gt]: start_time
                    }
                },
                include: [
                    {
                        model: order_detail,
                        attributes: ['order_detail_id', 'offer_price'],
                        include: [
                            {
                                model: service_detail,
                                attributes: {exclude: ['updatedAt', 'createdAt', 'service_id']},
                                include: [
                                    {
                                        model: service,
                                        attributes: ['service_name']
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        model: customer,
                        attributes: ['customer_id'],
                        include: [
                            {
                                model: user,
                                attributes: ['fullName']
                            }
                        ]
                    }
                ]
            })
            return res.send({ code: 200, data: foundedOrder });
        } catch (err) {
            return res.send({ code: 500, msg: "Internal Server Error", err })
        }
    }
}

module.exports = OrderController