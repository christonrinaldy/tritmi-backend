const {service} = require("../models")
class ServiceController {
    static async getAllServices(req, res) {
        try {
            const data = await service.findAll({attributes: {
                exclude: ['createdAt', 'updatedAt']
            }});
            return res.send({code: 200, msg: "success", data: data});
        } catch(error) {
            return res.send({code: 500, msg: "Internal Server Error", error});
        }
        
    }
    static async addService(req, res) {
        const { service_name } = req.body;

        function generateServiceId(val) {
            return new String(val).toLowerCase().split(/\s\s+/g).join("_").replace(" ", "_")
        }
        try {

            const foundedService = await service.findOne({where: {service_name}});
            if (!foundedService) {
                try {
                    var service_id = generateServiceId(service_name);
                    req.body.service_id = service_id
                    const addedService = await service.create(req.body);
                    return res.send({ code: 200, msg: "success", data: addedService });
                } catch (err) {
                    return res.send({code: 500, msg: "Internal Server Error", error: err.name})
                }
            } else {
                try {
                    return res.send({code: 401, msg: "service is already exist"})

                } catch (err) {
                    return res.send({ code: 500, msg: "Internal Server Error",error: err});
                }
            }

        } catch (err) {
            res.send({ code: 500, msg: err })
        }
    }

    static async deleteService(req, res) {
        const {service_id} = req.params;

        const foundService = await service.findByPk(service_id);
        if(!foundService) {
            return res.send({code: 404, msg: "not found"});
        }
        try {
            const deletedService = await service.destroy({where: {service_id}});
            return res.send({code: 200, msg: "success"})
        } catch(err) {
            res.send({code: 500, msg: "Internal Server Error", error: err})
        }
    }
    static async updateService(req, res) {
        const {service_id} = req.params;
        const foundServices = await service.findByPk(service_id);
        if(!foundServices) {
            return res.send({code: 404, msg: "not found"});
        }   
        try {
            const updatedServices = await service.update(req.body, {where: {service_id}});
            return res.send({code: 200, msg: "success"})
        } catch(err) {
            return res.send({code: 500, msg: err})
        }
    }
}

module.exports = ServiceController;
