const { business_partner, userPartner, user, role, order, businessPartner_customer, customer } = require('../models');
const uuidv4 = require("uuid");
const businesspartner_customer = require('../models/businesspartner_customer');

class BusinessPartnerController {
    static async getAll(req, res) {
        const {business_partner_id} = req.params
        try {
            const foundBusinessPartner = await business_partner.findAll();
            if (!allSaloons) {
                return res.send({ code: 404, msg: "no saloons are found" })
            }
            return res.send({ code: 200, msg: "success", data: foundBusinessPartner })
        } catch (err) {
            res.send({ code: 500, msg: "Internal server error" + err })
        }
    }
    static async getBusinessPartnerById(req, res) {
        const { business_partner_id } = req.params;
        const option = {
            where: { business_partner_id },
            include: [{
                model: userPartner,
                include: [{
                    model: role,
                    attributes: ['role_id', 'role_name'],
                    required: false
                }, {
                    model: user,
                    attributes: ['user_id', 'email', 'first_name', 'last_name', 'birthday', 'address'],
                }],
            }]
        }

        try {
            const foundBusinessPartner = await business_partner.findOne(option);
            if (foundBusinessPartner) {
                return res.send({ code: 200, msg: "success", data: foundBusinessPartner })
            } else {
                return res.send({ code: 404, msg: "Not Found" })
            }
        } catch (err) {
            res.send({ code: 500, msg: "Internal server error" + err })
        }

    }
    static async createbusinessPartner(req, res) {
        const { manager_id} = req.body;
        try {
            //
            const foundBusinessPartner = await business_partner.findAll(
                { 
                    where: {manager_id}
                }
            )
            if (foundBusinessPartner.length == 0) {

                const business_partner_id = uuidv4.v4();

                try {
                    req.body.business_partner_id = business_partner_id;
                    const createdBusinessPartner = await business_partner.create(req.body);

                    await userPartner.update({ business_partner_id }, { where: { user_partner_id: manager_id } });
                    return res.send({ code: 200, msg: "success", data: createdBusinessPartner });
                } catch (err) {
                    await business_partner.destroy({where: {business_partner_id}});
                    await order.create()
                    return res.send({ code: 500, msg: "Internal Server Error: userPartner", error: err })
                }

            } else {
                return res.send({ code: 409, msg: "Manager is already registered on other business partner" });
            }
        } catch (err) {
            return res.send({ code: 500, msg: "Internal Server Error", error: err })
        }
    }
    static async updateBusinessPartnerById(req, res) {
        const {business_partner_id} = req.params;
        const {manager_id} = req.body;
        try {
            const foundBusinessPartner = await business_partner.findOne({ where: {business_partner_id} })
            if (!foundBusinessPartner) {
                return res.send({ code: 404, msg: "not found" })
            }

            if(manager_id !== undefined) {
                const foundUserPartner = await business_partner.findOne({where: {manager_id}})
                if(foundUserPartner) {
                    return res.send({code: 409, msg: "manager_id is already used"})
                } 
                await userPartner.update({business_partner_id}, {where: {user_partner_id: manager_id}})
            }
            await business_partner.update(req.body, {where: {business_partner_id}});

            const updatedBusinessPartner = await business_partner.findOne({where: {business_partner_id}})
            res.send({code: 200, msg: "success", data: updatedBusinessPartner})
        } catch (err) {
            res.send({code: 500, msg: "Internal Server Error", error: err})
        }
    }
    static async deleteBusinessPartnerById(req, res) {

        const { business_partner_id } = req.params;
        try {
            const foundBusinessPartner = await business_partner.findOne({ where: {business_partner_id} })
            if (!foundBusinessPartner) {
                return res.send({ code: 404, msg: "not found" })
            }
            await business_partner.destroy({where: {business_partner_id}});
            res.send({ code: 200, msg: `success`, data: foundBusinessPartner });
        } catch (err) {
            res.send({ code: 500, msg: err })
        }
    }
// Relation to Customer
    static async addCustomerToBusinessPartner(req, res) {
        try {
            const {customer_id} = req.body;
            const {business_partner_id} = req.params;
            await businessPartner_customer.create({
                customer_id, business_partner_id
            });

            const option = {
                where: { customer_id },
                include: [{
                    model: user
                }]
            }
            const addedCustomerToBusinessPartner = await customer.findOne(option);
            delete addedCustomerToBusinessPartner.dataValues.user.dataValues.password;

            res.send({code: 200, msg: "success", data: addedCustomerToBusinessPartner})
            
        } catch(err) {
            res.send({code: 500, msg: "Internal Server Error", error: err})
        }
    }
    static async getCustomerPartnersByBusinessPartnerId(req, res) {
        try {
            const {business_partner_id} = req.params;
            var result = await businessPartner_customer.findAll({
                where: {business_partner_id},
                attributes: ['id'],
                include: [
                    {
                        model: customer,
                        attributes: ["customer_id", "is_valid", "user_id"],
                        include: [
                            {
                                model: user,
                                attributes: ["first_name", "last_name", "email", "phone_number"]
                            }
                        ]
                    }
                ]
            })
            res.send({code: 200, msg: "success", data: result})
        } catch(err) {
            return res.send({code: 500, msg: "Internal Server Error", error: err})
        }
    }
    static async getCustomerPartnerByBusinessPartnerIdAndCustomerId(req, res) {
        try {
            const {business_partner_id, customer_id} = req.params;
            const result = await businessPartner_customer.findOne({
                where: {business_partner_id, customer_id},
                attributes: ['id'],
                include: [
                    {
                        model: customer,
                        include: [
                            {
                                model: user,
                            }
                        ]
                    }
                ]
            })
            delete result.dataValues.customer.dataValues.user.dataValues.password
            res.send({code: 200, msg: "success", data: result})
        } catch(err) {
            return res.send({code: 500, msg: "Internal Server Error", error: err})
        }
    }    
    static async delCustomerPartnerByBusinessPartnerIdAndCustomerId(req, res) {
        try {
            await businessPartner_customer.destroy({where: req.body})
            res.send({code: 200, msg: "success"})
        } catch(err) {
            res.send({code: 500, msg: "Intenal Server Error", error: err})
        }
    }
}
module.exports = BusinessPartnerController;