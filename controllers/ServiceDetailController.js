const uuidv4 = require("uuid");
const { service_detail, service } = require("../models");

class ServiceDetailController {
    static async getAllServicesDetailsByBusinessPartnerId(req, res, next) {
        const { business_partner_id } = req.params;
        try {
            const data = await service_detail.findAll({
                where: { business_partner_id },
                include: [{
                    model: service
                }]
            });

            return res.send({ code: 200, msg: "success", data: data });
        } catch (err) {
            next(err)
        }


    }
    static async addServiceDetailsToBusinessPartnerId(req, res, next) {
        const service_detail_id = uuidv4.v4();
        req.body["service_detail_id"] = service_detail_id
        try {
            const addedService = await service_detail.create(req.body);
            return res.send({ code: 200, msg: "success", data: addedService });
            // // const foundedService = await service_detail.findOne({ where: { service_id, business_partner_id } });
            // if (true) {
            //         const addedService = await service_detail.create(req.body);
            //         return res.send({ code: 200, msg: "success", data: addedService });
            // } else {
            //     try {
            //         // return res.send({ code: 401, msg: "service is already exist" })

            //     } catch (err) {
            //         return res.send({ code: 500, msg: "Internal Server Error", error: err });
            //     }
            // }
        } catch (err) {
            next(err)
        }
    }

    static async deleteServiceDetailsByServiceDetailsId(req, res) {

        try {
            const { service_detail_id } = req.params;
            const foundService = await service_detail.findByPk(service_detail_id);
            if (!foundService) {
                return res.send({ code: 404, msg: "not found" });
            }
            try {
                const deletedService = await service_detail.destroy({ where: { service_detail_id } });
                return res.send({ code: 200, msg: "success", data: foundService })
            } catch (err) {
                res.send({ code: 500, msg: "Internal Server Error", error: err })
            }
        } catch (err) {
            res.send({ code: 500, msg: "Internal Server Error", error: err })
        }

    }
    static async updateServiceDetailsByServiceDetailId(req, res, next) {
        // const foundServices = await service_detail.findByPk(service_detail_id);
        // if (!foundServices) {
        //     return res.send({ code: 404, msg: "not found" });
        // }
        try {
            const { service_detail_id } = req.params;
            await service_detail.update(req.body, { where: { service_detail_id } });
            const updatedserviceDetail = await service_detail.findOne({ where: { service_detail_id } })

            return res.send({ code: 200, msg: "success", data: updatedserviceDetail })
        } catch (err) {
            next()
        }
    }
}
module.exports = ServiceDetailController;