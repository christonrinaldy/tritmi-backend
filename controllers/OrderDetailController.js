const { service_detail, order_detail, order, cart } = require("../models")
const uuidv4 = require("uuid");

class OrderDetailController {
    static async addOrderDetail(req, res, next) {
        try {
            const { customer_id, service_id, business_partner_id, service_detail_id, base_price } = req.body;
            const order_detail_id = uuidv4.v4();
            //cari cart_id customer
            const serviceDetailsRaw = await service_detail.findOne({
                where: {
                    service_detail_id, business_partner_id
                },
                attributes: ['offer_price']
            })
            if(serviceDetailsRaw == null || serviceDetailsRaw == undefined) {
                try {
                    var error  = new Error()
                    error.name = "not found"
                    throw error
                } catch(err) {
                    return next(err)
                }
            }

            if(typeof(base_price) !== "number") {
                req.body["base_price"] = serviceDetailsRaw.dataValues.offer_price
            }

            const cartVal = await cart.findOne({
                where: { customer_id },
                attributes: ['cart_id', 'business_partner_id'],
                include: [{
                    model: order_detail
                }]
            })
            const cart_id = cartVal.dataValues.cart_id;
            req.body.cart_id = cartVal.dataValues.cart_id;
            req.body.order_detail_id = order_detail_id;

            await order_detail.create(req.body);
            if(cartVal.dataValues.business_partner_id != business_partner_id) {
                cart.update({business_partner_id}, {where: {cart_id}})
                    .then((result) => {
                        
                    }).catch((err) => {
                        
                    });
                const order_detail_ids = cartVal.dataValues.order_details.map(val => val.order_detail_id);
                await order_detail.update({cart_id: null}, {where: {order_detail_id: order_detail_ids}})

            }
            const data = await order_detail.findAll({where: {cart_id}})

            res.send({ code: 200, msg: "success", data})

        } catch (err) {
            next(err)
        }
    }
    //update many order details order_id - create an order
    static async updateOrderDetailsOrderId(req, res) {
        try {

        } catch (err) {

        }
    }


    static async deleteOrderDetailById(req, res) {
        const { order_detail_id } = req.params;

        try {
            const foundedOrderDetail = await order_detail.findOne({ where: { order_detail_id } });
            if (foundedOrderDetail) {
                try {
                    const deletedOrderDetail = await order_detail.destroy({ where: { order_detail_id } })

                    return res.send({ code: 200, msg: "success", data: foundedOrderDetail })
                } catch (error) {
                    return res.send({ code: 500, msg: "Internal Server Error", error })
                }
            } else {
                return res.send({ code: 404, msg: "not found" })
            }

        } catch (err) {
            return res.send({ code: 500, msg: "Internal Server Error", error: err })
        }
    }

    static async updateOrderDetailById(req, res) {
        try {
            const { order_detail_id } = req.params;
            const foundedOrderDetail = await order_detail.findOne({ where: { order_detail_id } });
            if (foundedOrderDetail) {
                try {
                    await order_detail.update(req.body, { where: { order_detail_id } })
                    const updatedOrderDetail = await order_detail.findOne({ where: { order_detail_id } })

                    return res.send({ code: 200, msg: "success", data: updatedOrderDetail })
                } catch (error) {
                    return res.send({ code: 500, msg: "Internal Server Error", error })
                }
            } else {
                return res.send({ code: 404, msg: "not found" })
            }

        } catch (err) {
            return res.send({ code: 500, msg: "Internal Server Error" })
        }
    }
    static async getOrderDetailByOrderId(req, res) {
        const { order_id } = req.params;
        try {
            const foundedOrderDetail = await order_detail.findAll({ where: { order_id } });
            if (foundedOrderDetail) {
                return res.send({ code: 200, msg: "success", data: foundedOrderDetail })
            } else {
                return res.send({ code: 404, msg: "not found" })
            }

        } catch (err) {
            return res.send({ code: 500, msg: "Internal Server Error", error: err })
        }
    }
    static async updateMultipleOrderDetailsOrderId(req, res) {
        try {
            const { order_detail_ids, order_id } = req.body;
            const foundedOrder = await order.findOne({ where: { order_id } });
            if (foundedOrder) {
                const foundedOrderDetails = await order_detail.findAll({
                    where: {
                        order_detail_id: order_detail_ids
                    }
                })
                if (foundedOrderDetails.length > 0) {
                    foundedOrderDetails.map((obj) => {
                        if (obj.order_id !== null) {
                            return res.send({ code: 401, msg: `${obj.order_detail_id} is already registered in other order` })
                        }
                    })
                    await order_detail.update({ order_id }, { where: { order_detail_id: order_detail_ids } })
                    const updatedOrderDetails = await order_detail.findAll({ where: { order_detail_id: order_detail_ids } })
                    res.send({ code: 200, data: updatedOrderDetails })
                } else {
                    res.send({ code: 404, msg: "order detail(s) not found" })
                }
            } else {
                return res.send({ code: "404", msg: "order not found" })
            }
        } catch (err) {
            return res.send({ code: 500, msg: "Internal Server Error" })
        }
    }
    //ngga dipake harusnya
    static async createMultipleOrderDetails(req, res, next) {

        try {
            async function setToCart() {

            }
            const { orderDetailsRaw, order_id, customer_id } = req.body;
            if (orderDetailsRaw.length == 0) {
                return res.send({ code: 401, msg: "You must choose at least one service" })
            }
            var service_detail_ids = [];
            Object.assign(service_detail_ids, orderDetailsRaw.map((val) => val.service_detail_id));

            var total_order_price = 0

            const serviceDetailsRaw = await service_detail.findAll({
                where: {
                    service_detail_id: service_detail_ids
                }
            })

            orderDetailsRaw.forEach((obj, i) => {
                const order_detail_id = uuidv4.v4();
                obj.order_detail_id = order_detail_id;
                obj.order_id = order_id;
                obj.base_price = serviceDetailsRaw.filter(obj1 => obj1.service_detail_id == obj.service_detail_id)[0].offer_price;
                obj.offer_price = new Number(obj.base_price);
                total_order_price += obj.offer_price
            })

            try {
                const foundedOrderDetail = await order_detail.findAll({
                    where: {
                        service_detail_id: service_detail_ids,
                        order_id,
                        customer_id
                    }
                })
                if (false) {
                    res.send({ code: 401, msg: `Service detail id(s): ${foundedOrderDetail.map((obj) => obj.service_detail_id)} and order_id: ${order_id}, are already exist`, data: foundedOrderDetail })
                } else {
                    const createdOrderDetails = await order_detail.bulkCreate(orderDetailsRaw);
                    await order.update({ base_price: total_order_price, offer_price: total_order_price }, {
                        where: {
                            order_id
                        }
                    })
                    res.send({ code: 200, msg: "success", data: createdOrderDetails })

                }
            } catch (err) {
                next(err)
            }
        } catch (err) {
            next(err)
        }

    }
}

module.exports = OrderDetailController