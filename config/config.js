const fs = require('fs');
require('dotenv').config()
module.exports = {
    development: {
        username: String(process.env.USERNAME_DEV),
        password: String(process.env.PASSWORD_DEV),
        database: String(process.env.DB_DEV),
        host: process.env.HOST_DEV,
        dialect: 'postgres'
      },
      test: {
        username: String(process.env.USERNAME_TEST),
        password: String(process.env.PASSWORD_TEST),
        database: String(process.env.DB_TEST),
        host: process.env.HOST_TEST,
        dialect: 'postgres'
      },
      production: {
        username: String(process.env.USERNAME_PROD),
        password: String(process.env.PASSWORD_PROD),
        database: String(process.env.DB_PROD),
        host: process.env.HOST_PROD,
        dialect: 'postgres'
      }
};