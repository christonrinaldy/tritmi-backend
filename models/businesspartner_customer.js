'use strict';
const uuidv4 = require("uuid");
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class businessPartner_customer extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.business_partner.belongsToMany(models.customer, {through: businessPartner_customer, foreignKey: "business_partner_id", otherKey: "customer_id"});
      models.customer.belongsToMany(models.business_partner, {through: businessPartner_customer, foreignKey: "customer_id", otherKey: "business_partner_id"});

      models.business_partner.hasMany(businessPartner_customer, {foreignKey: "business_partner_id"});
      businessPartner_customer.belongsTo(models.business_partner, {foreignKey: "business_partner_id"});

      models.customer.hasMany(businessPartner_customer, {foreignKey: "customer_id", sourceKey: "customer_id"});
      businessPartner_customer.belongsTo(models.customer, {foreignKey: "customer_id"})

    }
  };
  businessPartner_customer.init({
    customer_id: DataTypes.STRING,
    business_partner_id: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'businessPartner_customer'
  });
  return businessPartner_customer;
};