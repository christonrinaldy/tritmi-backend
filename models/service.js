'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class service extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      service.belongsTo(models.service_detail, {foreignKey: "service_id"});
    }
  };
  service.init({
    service_id: {type: DataTypes.STRING, primaryKey: true},
    service_name: {type: DataTypes.STRING, unique: true},
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'service',
    hooks: {
      beforeCreate: (instance, option) => {
        instance.createdBy == undefined ? instance.createdBy = "TRITMI" :true;
        instance.updatedBy == undefined ? instance.updatedBy = "TRITMI" :true;
      }
    }
  });
  return service;
};