'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class userPartner extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      userPartner.hasOne(models.user, {foreignKey: "user_id", sourceKey: "user_id"});

      userPartner.belongsTo(models.business_partner, {
        foreignKey: 'business_partner_id',
        targetKey: 'business_partner_id',
      });
      // userPartner.belongsTo(models.business_partner, {
      //   foreignKey: 'manager_id',
      // })

      userPartner.belongsTo(models.role, {foreignKey: 'role_id'});
    }
  };
  userPartner.init({
    user_partner_id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    role_id: {type: DataTypes.INTEGER},
    business_partner_id: {
      type: DataTypes.STRING,
    },
    user_id: {
      type: DataTypes.STRING
    },
    email: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'userPartner',
  });
  return userPartner;
};