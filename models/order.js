'use strict';
const {
  Model, or,
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      order.hasMany(models.order_detail, {foreignKey: 'order_id', sourceKey: 'order_id'});
      order.hasOne(models.customer, {foreignKey: 'customer_id', sourceKey: 'customer_id'})
      // order.belongsTo(models.order_detail, {foreignKey: "order_id"})
    }
  };
  order.init({
    order_id: {type: DataTypes.STRING, primaryKey: true},
    order_time: {type: DataTypes.DATE, defaultValue: new Date(), allowNull: false},
    business_partner_id: {type: DataTypes.STRING, allowNull: false},
    customer_id: {type: DataTypes.STRING, allowNull: false},
    offer_price: DataTypes.INTEGER,
    base_price: DataTypes.STRING,
    customer_rating: DataTypes.FLOAT,
    business_partner_rating: DataTypes.FLOAT,
    created_by: DataTypes.STRING,
    updated_by: DataTypes.STRING
  }, {
    sequelize,
    hooks: {
      beforeCreate: (instance, option) => {
        if(instance.offer_price == null) {
          instance.offer_price = instance.base_price
        }
        if(instance.created_by == null) {
          instance.created_by = "TRITMI"
        }
        if(instance.updated_by == null) {
          instance.updated_by = "TRITMI"
        }
      }
    },
    modelName: 'order',
  });
  return order;
};