'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class order_detail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      order_detail.belongsTo(models.order, {foreignKey: "order_id", targetKey: "order_id"})
      order_detail.hasOne(models.service_detail, {foreignKey: 'service_detail_id', sourceKey: 'service_detail_id'})
    }
  };
  order_detail.init({
    order_detail_id: { type: DataTypes.STRING, primaryKey: true },
    service_detail_id: { type: DataTypes.STRING },
    order_id: DataTypes.STRING,
    offer_price: { type: DataTypes.INTEGER },
    base_price: { type: DataTypes.INTEGER },
    customer_id: { type: DataTypes.STRING, allowNull: false },
    is_deleted: {type: DataTypes.BOOLEAN}
  }, {
    hooks: {
      beforeCreate: (instance, option) => {
        if (instance.getDataValue("offer_price") == null || instance.getDataValue("offer_price") == undefined)
          instance.setDataValue("offer_price", instance.getDataValue("base_price"))
      },
      beforeUpdate: (instane, option) => {
        console.log("before update")
        if((instane.getDataValue("order_id") == null || instane.getDataValue("order_id") == undefined) && instane.getDataValue("cart_id") == null) {
          instane.setDataValue("is_deleted", true)
        }
      },
      beforeBulkUpdate: (option) => {
        console.log("before bulk update")
        option.individualHooks = true
      }
      
    },
    sequelize,
    modelName: 'order_detail',
  });
  return order_detail;
};