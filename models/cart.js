'use strict';
const uuidv4 = require("uuid");

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class cart extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      cart.hasMany(models.order_detail, {foreignKey: 'cart_id', sourceKey: 'cart_id'})
    }
  };
  cart.init({
    cart_id: {
      type: DataTypes.STRING, primaryKey: true
    },
    customer_id: DataTypes.STRING,
    business_partner_id: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'cart',
    tableName: 'carts',
    hooks: {
      beforeCreate: (instance, option) => {
        instance.setDataValue('cart_id', uuidv4.v4());
      }
    }
  });
  return cart;
};