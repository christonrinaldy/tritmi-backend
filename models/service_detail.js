'use strict';
const uuidv4 = require("uuid");
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class service_detail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      service_detail.belongsTo(models.service, {foreignKey: "service_id"});
      service_detail.belongsTo(models.business_partner, {
        foreignKey: 'business_partner_id',
        targetKey: 'business_partner_id'
      })
    }
  };
  service_detail.init({
    service_detail_id: {type: DataTypes.STRING, primaryKey: true},
    service_id: DataTypes.STRING,
    business_partner_id: DataTypes.STRING,
    base_price: {type: DataTypes.INTEGER, allowNull: false},
    offer_price: {type: DataTypes.INTEGER},
    min_duration: {type: DataTypes.INTEGER, allowNull: false},
    max_duration: {type: DataTypes.INTEGER, allowNull: false},
    description: DataTypes.STRING
  }, {
    sequelize,
    hooks: {beforeCreate: (instance, option) => {
      instance.offer_price == null ? (instance.offer_price = instance.base_price) : null;
    }},
    indexes: [
      {
        unique: true,
        fields: ['service_id', 'business_partner_id']
      }
    ],
    modelName: 'service_detail',
  });
  return service_detail;
};