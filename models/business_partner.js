'use strict';
const uuid = require('uuid');
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class business_partner extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      business_partner.hasMany(models.service_detail, {
        foreignKey: "business_partner_id",
        sourceKey: "business_partner_id"
      });
      business_partner.hasMany(models.userPartner, {
        foreignKey: 'business_partner_id',
        sourceKey: "business_partner_id"
      });
    }
  };
  business_partner.init({
    business_partner_id: {type: DataTypes.STRING, primaryKey: true},
    name: DataTypes.STRING,
    lattitude: DataTypes.STRING,
    longitude: DataTypes.STRING,
    province: DataTypes.STRING,
    city: DataTypes.STRING,
    district: DataTypes.STRING,
    owner_id: DataTypes.STRING,
    pic: DataTypes.STRING,
    is_valid: DataTypes.BOOLEAN,
    manager_id: DataTypes.STRING,
    phone_number: DataTypes.STRING,
    address: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'business_partner',
    hooks: {
      
    }
  });
  return business_partner;
};