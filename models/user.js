'use strict';
const {
  Model
} = require('sequelize');
const bcrypt = require('bcrypt');
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // user.hasOne(models.userPartner, {foreignKey: 'user_id'});
      user.belongsTo(models.customer, { foreignKey: "customer_id", targetKey: "customer_id" });

      user.belongsTo(models.userPartner, {
        foreignKey: 'user_partner_id',
        targetKey: "user_partner_id"
      });
      // user.belongsTo(models.customer, {
      //   onDelete: 'cascade',
      //   onUpdate: 'cascade',
      //   foreignKey: 'customer_id'
      // });
    }
  };
  user.init({
    user_id: {
      primaryKey: true,
      type: DataTypes.STRING
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: "password should not empty"
        },
        notEmpty: {
          msg: "password should not empty"
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: "password should not empty"
        },
        notEmpty: {
          msg: "password should not empty"
        }
      }
    },
    first_name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: "First name should not empty"
        },
        notEmpty: {
          msg: "First name should not empty"
        }
      }
    },
    last_name: DataTypes.STRING,
    birthday: DataTypes.DATEONLY,
    address: {
      type: DataTypes.STRING
    },
    user_partner_id: {
      type: DataTypes.STRING
    },
    customer_id: {
      type: DataTypes.STRING
    },
    phone_number: {
      type: DataTypes.STRING
    },
    fullName: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'user',
    hooks: {
      beforeCreate: (instance, option) => {
        try {
          instance.setDataValue("password", bcrypt.hashSync(instance.getDataValue("password"), 10));
          const fullName = instance.getDataValue("first_name") + " " + instance.getDataValue("last_name");
          instance.dataValues.fullName = fullName;
        } catch (err) {
          throw new Error(err)
        }
      },

    }
  });
  return user;
};