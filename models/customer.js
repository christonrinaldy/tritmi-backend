'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class customer extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      customer.hasOne(models.user, {foreignKey: 'user_id', sourceKey: "user_id"});
    }
  };
  customer.init({
    customer_id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    is_valid: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    user_id: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'customer',
    
  });
  return customer;
};