'use strict';
const uuidv4 = require("uuid");
const bcrypt = require("bcrypt");

const password = process.env.CUSTOMER_PASSWORD
const email = process.env.CUSTOMER_EMAIL

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const user_id = await uuidv4.v4();
    const customer_id = await uuidv4.v4();

    await queryInterface.bulkInsert('users', [
      {
        user_id,
        first_name: "John",
        last_name: "Doe",
        email: email,
        address: "Jalan Sesama no 1",
        password: await bcrypt.hash(password, 5),
        // customer_id,
        createdAt: new Date(),
        updatedAt: new Date(),
      }
    ], {})
    await queryInterface.bulkInsert('customers', [
      {
        user_id,
        customer_id,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {

    })
    await queryInterface.bulkUpdate('users', 
      {customer_id},
      {user_id}
    )
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
