'use strict';
require('dotenv').config({ path: './.env' })
const uuidv4 = require("uuid");
const bcrypt = require("bcrypt");

const password = process.env.USER_PARTNER_PASSWORD
const email = process.env.USER_PARTNER_EMAIL

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   const user_id = await uuidv4.v4();
   const user_partner_id = await uuidv4.v4();
    await queryInterface.bulkInsert('users', [
      {
        user_id: user_id,
        email: email,
        password: await bcrypt.hash(password, 5),
        first_name: "John",
        last_name: "Doe",
        address: "Jalan Sesama",
        // user_partner_id: user_partner_id,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {})
    await queryInterface.bulkInsert('userPartners',[
      {
        user_partner_id: user_partner_id,
        role_id: 2,
        user_id: user_id,
        email: email,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {})
    await queryInterface.bulkUpdate('users', 
      {user_partner_id},
      {user_id}
    )
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('users', null, {});
    await queryInterface.bulkDelete('userPartners', null, {});
  }
};
