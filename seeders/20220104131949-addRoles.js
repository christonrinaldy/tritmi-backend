'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('roles', [
      {
        role_id: 1,
        role_name: "owner",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        role_id: 2,
        role_name: "manager",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        role_id: 3,
        role_name: "staff",
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('roles', null, {})
  }
};
