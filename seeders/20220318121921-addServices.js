'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('services', [
      {
        service_id: "cucigunting",
        service_name: "Cuci dan Gunting",
        createdBy: "TRITMI", 
        updatedBy: "TRITMI",
        createdAt: new Date(),
        updatedAt: new Date()
        
      },
      {
        service_id: "gunting",
        service_name: "Gunting",
        createdBy: "TRITMI", 
        updatedBy: "TRITMI",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        service_id: "creambath",
        service_name: "Creambath",
        createdBy: "TRITMI", 
        updatedBy: "TRITMI",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        service_id: "pijat_refleksi",
        service_name: "Pijat dan Refleksi",
        createdBy: "TRITMI", 
        updatedBy: "TRITMI",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {})
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
      await queryInterface.bulkDelete('services', null, {})
  }
};
