'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('customers', 'is_valid', { type: Sequelize.BOOLEAN, default: false });
    await queryInterface.addColumn('userPartners', 'is_valid', { type: Sequelize.BOOLEAN, default: false });

  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('customers', 'is_valid', {});
    await queryInterface.removeColumn('userPartners', 'is_valid', {});
  }
};
