'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('users', 'user_partner_id', { type: Sequelize.STRING });
    await queryInterface.addColumn('users', 'customer_id', { type: Sequelize.STRING });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('users', 'user_partner_id');
    await queryInterface.removeColumn('users', 'customer_id');
  }
};
