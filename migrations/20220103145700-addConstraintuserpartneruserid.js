'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addConstraint('userPartners', {
      fields: ['user_id'],
      type: 'foreign key',
      name: 'fkey_userpartner-user',
      references: { //Required field
        table: 'users',
        fields: ['user_id']
      },
      onDelete: 'cascade',
      onUpdate: 'cascade'
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('userPartners', 'fkey_userpartner-user')
  }
};
