'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('business_partners', 'address', { type: Sequelize.STRING });

  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('business_partners', 'address');
  }
};
