'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addConstraint('service_details', {
      fields: ['business_partner_id'],
      type: 'foreign key',
      name: 'fkey_business_partner-service_details',
      references: { //Required field
        table: 'business_partners',
        fields: ['business_partner_id']
      },
      onDelete: 'cascade',
      onUpdate: 'cascade'
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('service_details', 'fkey_business_partner-service_details');
  }
};
