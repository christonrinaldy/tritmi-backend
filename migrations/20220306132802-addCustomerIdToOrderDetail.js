'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('order_details', 'customer_id', { type: Sequelize.STRING });
    await queryInterface.addConstraint('order_details', {
      fields: ['customer_id'],
      type: 'foreign key',
      name: 'fkey_customer-order_details',
      references: { //Required field
        table: 'customers',
        fields: ['customer_id']
      },
      onDelete: 'cascade',
      onUpdate: 'cascade'
    })

  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('order_details', 'fkey_customer-order_details')
    await queryInterface.removeColumn('order_details', 'customer_id');

  }
};
