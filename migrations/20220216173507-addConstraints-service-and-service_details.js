'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addConstraint('service_details', {
      fields: ['service_id'],
      type: 'foreign key',
      name: 'fkey_service-service_details',
      references: { //Required field
        table: 'services',
        fields: ['service_id']
      },
      onDelete: 'cascade',
      onUpdate: 'cascade'
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('service_details', 'fkey_service-service_details');
  }
};
