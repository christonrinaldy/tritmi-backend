'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addConstraint('userPartners', {
      fields: ['user_partner_id'],
      type: 'primary key',
      name: 'user_partner_id'
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('userPartners', 'user_partner_id')
  }
};
