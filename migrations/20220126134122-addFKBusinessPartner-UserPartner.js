'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    try{
      await queryInterface.addConstraint('userPartners', {
        fields: ['business_partner_id'],
        type: 'foreign key',
        name: 'foreignkey-business_partners-userpartners',
        references: { //Required field
          table: 'business_partners',
          fields: ['business_partner_id']
        },
        onDelete: 'SET NULL',
        onUpdate: 'CASCADE'
      })
    } catch(err) {
      console.log(err);
    }
    
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('userPartners', 'foreignkey-business_partners-userpartners')
  }
};
