'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('order_details', 'is_deleted', {type: Sequelize.BOOLEAN})
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('order_details', 'is_deleted', {})
  }
};
