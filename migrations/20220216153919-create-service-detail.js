'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('service_details', {
      id: {
        allowNull: false,
        autoIncrement: true,
        type: Sequelize.INTEGER
      },
      service_detail_id: {
        primaryKey: true,
        type: Sequelize.STRING
      },
      service_id: {
        type: Sequelize.STRING
      },
      business_partner_id: {
        type: Sequelize.STRING
      },
      base_price: {
        type: Sequelize.INTEGER
      },
      offer_price: {
        type: Sequelize.INTEGER
      },
      min_duration: {
        type: Sequelize.INTEGER
      },
      max_duration: {
        type: Sequelize.INTEGER
      },
      description: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }, {
      uniqueKeys: {
        Items_unique: {
          fields: ['service_id', 'business_partner_id']
        }
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('service_details');
  }
};