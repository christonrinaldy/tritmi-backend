'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addConstraint('order_details', {
      fields: ['order_id'],
      type: 'foreign key',
      name: 'fkey_order-order_details',
      references: { //Required field
        table: 'orders',
        fields: ['order_id']
      },
      onDelete: 'cascade',
      onUpdate: 'cascade'
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('order_details', 'fkey_order-order_details');
  }
};
