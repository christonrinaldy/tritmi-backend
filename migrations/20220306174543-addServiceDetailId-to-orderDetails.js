'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addConstraint('order_details', {
      fields: ['service_detail_id'],
      type: 'foreign key',
      name: 'fkey_service_details-order_details',
      references: { //Required field
        table: 'service_details',
        fields: ['service_detail_id']
      },
      onDelete: 'cascade',
      onUpdate: 'cascade'
    })

  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('order_details', 'fkey_service_details-order_details')

  }
};
