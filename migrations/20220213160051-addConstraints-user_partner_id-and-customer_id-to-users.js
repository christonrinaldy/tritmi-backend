'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addConstraint('users', {
      fields: ['user_partner_id'],
      type: 'foreign key',
      name: 'fkey_user-user_partners',
      references: { //Required field
        table: 'userPartners',
        fields: ['user_partner_id']
      },
      onDelete: 'cascade',
      onUpdate: 'cascade'
    })
    await queryInterface.addConstraint('users', {
      fields: ['customer_id'],
      type: 'foreign key',
      name: 'fkey_user-customers',
      references: { //Required field
        table: 'customers',
        fields: ['customer_id']
      },
      onDelete: 'cascade',
      onUpdate: 'cascade'
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('users', 'fkey_user-user_partners');
    await queryInterface.removeConstraint('users', 'fkey_user-customers')
  }
};
