'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addConstraint('orders', {
      fields: ['business_partner_id'],
      type: 'foreign key',
      name: 'fkey_business_partner-orders',
      references: { //Required field
        table: 'business_partners',
        fields: ['business_partner_id']
      },
      onDelete: 'cascade',
      onUpdate: 'cascade'
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('orders', 'fkey_business_partner-orders');
  }
};
