'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addConstraint('customers', {
      fields: ['user_id'],
      type: 'foreign key',
      name: 'fkey_customeruser',
      references: { //Required field
        table: 'users',
        fields: ['user_id'],
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('customers', 'fkey_customeruser')
  }
};
