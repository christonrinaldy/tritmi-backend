'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addConstraint('business_partners', {
      fields: ['manager_id'],
      type: 'foreign key',
      name: 'fkey_userpartner-businesspartner',
      references: { //Required field
        table: 'userPartners',
        fields: ['user_partner_id']
      },
      onDelete: 'SET NULL',
      onUpdate: 'cascade'
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('business_partners', 'fkey_userpartner-businesspartner')
  }
};
