'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('businessPartner_customers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      customer_id: {
        type: Sequelize.STRING
      },
      business_partner_id: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }, {
      indexes: [
        {
          unique: true,
          fields: ['customer_id', 'business_partner_id']
        }
      ]
    });
    await queryInterface.addConstraint('businessPartner_customers', {
      fields: ['customer_id'],
      type: 'foreign key',
      name: 'fkey_businessPartner_customers-customers',
      references: { //Required field
        table: 'customers',
        fields: ['customer_id']
      },
      onDelete: 'cascade',
      onUpdate: 'cascade'
    });
    await queryInterface.addConstraint('businessPartner_customers', {
      fields: ['business_partner_id'],
      type: 'foreign key',
      name: 'fkey_businessPartner_customers-business_partners',
      references: { //Required field
        table: 'business_partners',
        fields: ['business_partner_id']
      },
      onDelete: 'cascade',
      onUpdate: 'cascade'
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('businessPartner_customers', 'fkey_businessPartner_customers-business_partners');
    await queryInterface.removeConstraint('businessPartner_customers', 'fkey_businessPartner_customers-customers');
    await queryInterface.dropTable('businessPartner_customers');
  }
};