'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('userPartners', 'user_id', { type: Sequelize.STRING });

  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('userPartners', 'user_id');

  }
};
