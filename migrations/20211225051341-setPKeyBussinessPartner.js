'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addConstraint('business_partners', {
      fields: ['business_partner_id'],
      type: 'primary key',
      name: 'business_partner_id'
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('business_partners', 'business_partner_id')
  }
};
