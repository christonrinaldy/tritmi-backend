'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('carts', {
      id: {
        allowNull: false,
        autoIncrement: true,
        type: Sequelize.INTEGER
      },
      cart_id: {
        type: Sequelize.STRING,
        allowNull: false,
        primaryKey: true,
      },
      customer_id: {
        type: Sequelize.STRING
      },
      business_partner_id: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }, {
      uniqueKeys: {
        actions_unique: {
          fields: ['customer_id']
        }
      }
    });
    await queryInterface.addConstraint('carts', {
      fields: ['business_partner_id'],
      type: 'foreign key',
      name: 'fkey_business_partner-carts',
      references: { //Required field
        table: 'business_partners',
        fields: ['business_partner_id']
      },
      onDelete: 'cascade',
      onUpdate: 'cascade'
    })
    await queryInterface.addConstraint('carts', {
      fields: ['customer_id'],
      type: 'foreign key',
      name: 'fkey_carts-customers',
      references: { //Required field
        table: 'customers',
        fields: ['customer_id']
      },
      onDelete: 'cascade',
      onUpdate: 'cascade'
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('carts', 'fkey_business_partner-carts')
    await queryInterface.removeConstraint('carts', 'fkey_carts-customers')
    await queryInterface.dropTable('carts');
  }
};