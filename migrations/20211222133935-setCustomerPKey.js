'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('customers', 'customers_pkey');
    await queryInterface.addConstraint('customers', {
      fields: ['customer_id'],
      type: 'primary key',
      name: 'pk_customer_id'
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('customers', 'pk_customer_id');
  }
};
