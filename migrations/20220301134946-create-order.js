'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('orders', {
      id: {
        allowNull: false,
        autoIncrement: true,
        type: Sequelize.INTEGER
      },
      order_id: {
        type: Sequelize.STRING,
        primaryKey: true,
      },
      order_time: {
        type: Sequelize.DATE
      },
      business_partner_id: {
        type: Sequelize.STRING
      },
      customer_id: {
        type: Sequelize.STRING
      },
      offer_price: {
        type: Sequelize.INTEGER
      },
      base_price: {
        type: Sequelize.STRING
      },
      customer_rating: {
        type: Sequelize.FLOAT
      },
      business_partner_rating: {
        type: Sequelize.FLOAT
      },
      created_by: {
        type: Sequelize.STRING
      },
      updated_by: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('orders');
  }
};