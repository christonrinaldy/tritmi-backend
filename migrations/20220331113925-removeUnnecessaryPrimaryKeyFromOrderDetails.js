'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('order_details', 'order_details_pkey')
    await queryInterface.addConstraint('order_details', {
      name: 'order_details_pkey',
      type: 'primary key',
      fields: ['order_detail_id']
    })
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
