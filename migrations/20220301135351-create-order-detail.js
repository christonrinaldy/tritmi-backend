'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('order_details', {
      id: {
        allowNull: false,
        autoIncrement: true,
        type: Sequelize.INTEGER
      },
      order_detail_id: {
        primaryKey: true,
        type: Sequelize.STRING
      },
      order_id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      service_detail_id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      service_id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      offer_price: {
        type: Sequelize.INTEGER
      },
      base_price: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('order_details');
  }
};