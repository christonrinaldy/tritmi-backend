'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('order_details', "cart_id", {type: Sequelize.STRING})
    await queryInterface.addConstraint('order_details', {
      fields: ['cart_id'],
      type: 'foreign key',
      name: 'fkey_carts-order_details',
      references: { //Required field
        table: 'carts',
        fields: ['cart_id']
      },
      onDelete: 'SET NULL',
      onUpdate: 'cascade'
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('order_details', 'fkey_carts-order_details')
    await queryInterface.removeColumn('order_details', "cart_id", {})
  }
};
