'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
      await queryInterface.addConstraint('userPartners', {
        fields: ['role_id'],
        type: 'foreign key',
        name: 'foreignkey-roles-userpartners',
        references: { //Required field
          table: 'roles',
          fields: ['role_id']
        },
        onDelete: 'cascade',
        onUpdate: 'cascade'
      })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('userPartners', 'foreignkey-roles-userpartners')
  }
};
