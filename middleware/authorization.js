function authorization(req,res,next){
    const {business_partner_id} = req.params;
    const {userLogin} = req;
    // console.log(business_partner_id, userLogin.business_partner_id);
    if(business_partner_id == userLogin.business_partner_id) {
        next()
    } else {
        return res.send({code: 401, msg: "not an authorized user"})
    }
}
module.exports=authorization