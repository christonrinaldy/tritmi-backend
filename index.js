require('dotenv').config({ path: './.env' })
const port = process.env.PORT
const express = require('express');
const { createServer } = require("http");
const { Server } = require("socket.io");


const cors = require('cors');
const route = require('./routes');

const app = express();
app.use(express.urlencoded({ extended: true }))
app.use(express.json());
app.use(cors());
app.use(route);

console.log("environment: ", process.env.NODE_ENV)
const server = createServer(app);
const io = new Server(server);

io.on('connection', (socketServer) => {
    console.log("socket server connected")
    socketServer.on('npmStop', () => {
        process.exit(0);
    });
});

server.listen(port, () => {
    console.log('app is listening to http://localhost:', port);
    
})

module.exports = app