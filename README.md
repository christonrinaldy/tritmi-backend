1. Download PgAdmin 4
2. Download and install npm on NodeJs
   npm install -g npm
3. Create file .env Tritmi\server. 

pada terminal di directory tritmi_backend ketik:

- untuk start pada mode development
   npm start
- untuk start pada mode testing
   npm run test
- untuk start pada mode production
   npm run production
- untuk stop aplikasi
   npm stop
** udah gaperlu create db, migrate dan seed