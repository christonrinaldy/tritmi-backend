const route = require('express').Router({mergeParams: true})
const UserController = require('../../controllers/userController');
const CustomerCartRoute = require('./CustomerCartRoute')
// const authorization = require('../middleware/authorization');
// const authentication = require('../middleware/authentication');

//getAll informasi umum
route.delete('/:user_id', UserController.deleteUserById);
route.put('/:user_id', UserController.updateUser);
route.get('/:email&:user_id', UserController.getUserByEmailOrUserId)
route.use('/customer/:customer_id/cart', CustomerCartRoute);

module.exports = route