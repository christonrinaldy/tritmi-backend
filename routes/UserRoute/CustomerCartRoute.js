const route = require('express').Router({mergeParams: true})
const CartController = require('../../controllers/CartController');

// const authorization = require('../middleware/authorization');
// const authentication = require('../middleware/authentication');

//getAll informasi umum
route.put('/', CartController.emptyCart);
route.get('/', CartController.getCartDetails)

module.exports = route