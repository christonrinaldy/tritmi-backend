const route = require('express').Router({mergeParams: true});
const OrderController = require('../../controllers/OrderController');

route.get('/times/:start_time&:end_time', OrderController.readOrderByBusinessPartnerIdAndTimeFilter);
module.exports = route;