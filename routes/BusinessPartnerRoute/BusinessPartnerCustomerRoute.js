const route = require('express').Router({mergeParams: true});
const BusinessPartnerController = require('../../controllers/BusinessPartner_Controller');
const UserController = require('../../controllers/userController');

route.get('/:customer_id', BusinessPartnerController.getCustomerPartnerByBusinessPartnerIdAndCustomerId);
route.get('/', BusinessPartnerController.getCustomerPartnersByBusinessPartnerId);
route.delete('/', BusinessPartnerController.delCustomerPartnerByBusinessPartnerIdAndCustomerId)
route.post('/', UserController.register, BusinessPartnerController.addCustomerToBusinessPartner);
module.exports = route;