const route = require('express').Router()
const BusinessPartnerController = require('../../controllers/BusinessPartner_Controller');
const BusinessPartnerRoute = require('./BusinessPartnerRoute')

route.use('/:business_partner_id', BusinessPartnerRoute)
route.post('/', BusinessPartnerController.createbusinessPartner);

module.exports = route;