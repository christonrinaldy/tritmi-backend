const route = require('express').Router({mergeParams: true});
const BusinessPartnerController = require('../../controllers/BusinessPartner_Controller');
const BusinessPartnerOrderRoute = require('./BusinessPartnerOrderRoute');
const BusinessPartnerCustomerRoute = require('./BusinessPartnerCustomerRoute');

route.put('/', BusinessPartnerController.updateBusinessPartnerById);
route.get('/', BusinessPartnerController.getBusinessPartnerById);
route.delete('/', BusinessPartnerController.deleteBusinessPartnerById);
route.use('/orders/', BusinessPartnerOrderRoute);
route.use('/customer/', BusinessPartnerCustomerRoute);

module.exports = route;