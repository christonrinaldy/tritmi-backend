const route = require('express').Router()
const OrderController = require('../controllers/OrderController');
const authorization = require('../middleware/authorization');
const authentication = require('../middleware/authentication');

route.post('/', OrderController.createOrder);
route.put('/:order_id', OrderController.updateOrderById);
route.get('/:order_id', OrderController.getOrderDetailByOrderId);
route.delete('/:order_id', OrderController.deleteOrderByOrderId)
module.exports = route;