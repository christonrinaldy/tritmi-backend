const route = require('express').Router()
const ServiceDetailController = require('../controllers/ServiceDetailController.js');



//getAll informasi umum
route.get('/:business_partner_id', ServiceDetailController.getAllServicesDetailsByBusinessPartnerId)
route.post('/', ServiceDetailController.addServiceDetailsToBusinessPartnerId);
route.delete('/:service_detail_id', ServiceDetailController.deleteServiceDetailsByServiceDetailsId)
route.put('/:service_detail_id', ServiceDetailController.updateServiceDetailsByServiceDetailId)

module.exports = route