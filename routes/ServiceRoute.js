const route = require('express').Router()
const ServiceController = require('../controllers/ServiceController.js');



//getAll informasi umum
route.get('/', ServiceController.getAllServices)
route.post('/', ServiceController.addService);
route.delete('/:service_id', ServiceController.deleteService)
route.put('/:service_id', ServiceController.updateService)

module.exports = route