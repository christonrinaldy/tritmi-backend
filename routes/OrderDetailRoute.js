const route = require('express').Router()
const OrderDetailController = require('../controllers/OrderDetailController');
const authorization = require('../middleware/authorization');
const authentication = require('../middleware/authentication');

route.post('/', OrderDetailController.addOrderDetail);
// route.post('/', OrderDetailController.createMultipleOrderDetails);
route.put('/', OrderDetailController.updateMultipleOrderDetailsOrderId)
route.delete('/:order_detail_id', OrderDetailController.deleteOrderDetailById);
route.put('/:order_detail_id', OrderDetailController.updateOrderDetailById);
route.get('/:order_id', OrderDetailController.getOrderDetailByOrderId);
module.exports = route;