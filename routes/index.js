const route = require('express').Router()
const BusinessPartner = require('./BusinessPartnerRoute/index');
const UserRoute = require('./UserRoute/index');
const ServiceRoute = require('./ServiceRoute');
const ServiceDetailRoute = require('./ServiceDetailRoute');
const OrderDetailRoute = require('./OrderDetailRoute');
const OrderRoute = require('./OrderRoute');
const UserController = require('../controllers/UserController');
const errorHandler = require('../middleware/error_handlers/index')

route.use('/service', ServiceRoute);
route.use('/businesspartner', BusinessPartner);
route.use('/order_detail', OrderDetailRoute);
route.use('/order', OrderRoute);
route.use('/service_detail', ServiceDetailRoute);
route.use('/user', UserRoute);
route.post('/login', UserController.login);
route.post('/register', UserController.register)
route.use(errorHandler)

route.get('/', (req, res) => {
    res.send({ code: 200, msg: "Welcome to Tritme_dev" })
})

module.exports = route